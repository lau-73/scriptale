﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

using static sTaleUtils;

namespace Assets.Scripts
{

    [ExecuteInEditMode]

    public static class Dialogue
    {
        public static List<Message> messages;

        public static void ImportMessages(List<string> dialogue)
        {
            messages = new List<Message>();

            Effect ms_effect = new Effect(EmotionType.None, 0.0f);

            for (int i = 0, intensity = 0; i < dialogue.Count; i++, intensity += 10)
            {
                if(dialogue[i][0] == '*')
                {
                    int c_remove = 0;
                    for( ; c_remove < dialogue[i].Length; ++c_remove)
                    {
                        if (char.IsLetter(dialogue[i][c_remove]))
                            break;
                    }

                    dialogue[i] = dialogue[i].Remove(0, c_remove);

                    ms_effect.emotion = (EmotionType)Enum.Parse(typeof(EmotionType), dialogue[i]);
                    intensity = 0;
                }
                else
                {
                    intensity = Mathf.Clamp(intensity, 0, 100);
                    ms_effect.level = intensity;
                    messages.Add(new Message(ms_effect, dialogue[i]));
                }
            }
            
        }

        //Returns all the messages expressing the specified emotion type
        public static List<Message> GetMessagesByEmotion(EmotionType emotion)
        {
            return messages.FindAll(m => m.effect.emotion == emotion);
        }

        //Retrieves the messages of a certain type of emotion of a level or approximated given a threshold
        public static List<Message> GetMessagesByEmotionAndLevel(EmotionType emotion, float level, float threshold)
        {
            return messages.FindAll(m => m.effect.emotion == emotion && (m.effect.level <= (level + threshold) && m.effect.level >= (level - threshold)));
        }
        
    }
}
