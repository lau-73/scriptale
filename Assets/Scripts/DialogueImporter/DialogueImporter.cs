﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Assets.Scripts;

namespace ScripTale
{
    [ExecuteInEditMode]
    public class DialogueImporter : MonoBehaviour
    {
        #region Consts

        private const string dialogue_file = @"Assets\Dialogues\DialogueSystem.txt";

        // Credentials are stored at ~/.STale_Credentials/QuickAccessCredentials.json
        // If any scope is modifyied, please delete the credentials folder
        private string[] scopes = { DriveService.Scope.Drive };
        private string app_name = "ScripTale";

        #endregion

        [Header("Import Dialogue")]
        public string documentName;

        public bool downloadDialogue = false;

        private bool download = false;

        private static List<string> dialogue_list;
        private DriveService service;

        [ContextMenu("Do Something")]
        void DoSomething()
        {
            Debug.Log("Perform operation");
        }

        public void OnValidate()
        {
            if (download == downloadDialogue) return;

            download = downloadDialogue;

            if (!downloadDialogue) return;
            
            dialogue_list = new List<string>();

            //Allow ScripTale to make requests to a server
            UnsafeSecurityPolicy.Instate();

            UserCredential credentials;

            //Set the credentials
            using (FileStream stream = new FileStream("client_credentials.json", FileMode.Open, FileAccess.Read))
            {
                string credential_path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                credential_path = Path.Combine(credential_path, @".STale_Credentials\QuickAccessCredentials.json");

                credentials = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credential_path, true)).Result;
                Debug.Log("You can find your quick access credential path at: " + credential_path);
            }

            //Create the Drive service
            service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credentials,
                ApplicationName = app_name,
            });

            ImportDialogue();

            download = downloadDialogue = false;
        }

        public void ImportDialogue()
        {
            
            try
            {
                using (FileStream file_stream = new FileStream(dialogue_file, FileMode.Open, FileAccess.Read))
                {

                    file_stream.Close();
                    System.IO.File.Delete(dialogue_file);
                    WriteDialogue();
                     
                    ReadDialogue();
                }
            }
            catch (FileNotFoundException e)
            {
                Debug.Log(e.Message);

                WriteDialogue();
                ReadDialogue();
            }
        }

        public void WriteDialogue()
        {

            FilesResource.ListRequest file_res = service.Files.List();
            string query = "name='" + documentName + "'";
            file_res.Q = query;
            file_res.Fields = "nextPageToken, files(id, name)";

            if (file_res == null) return;

            IList<Google.Apis.Drive.v3.Data.File> files = file_res.Execute().Files;

            if (files != null && files.Count > 0)
            {
                foreach (Google.Apis.Drive.v3.Data.File file in files)
                {
                    using (FileStream file_stream = new FileStream(dialogue_file, FileMode.Create, FileAccess.Write))
                    {

                        FilesResource.ExportRequest file_request = service.Files.Export(file.Id, "text/plain");

                        file_request.MediaDownloader.ProgressChanged += (progress) =>
                        {
                            switch (progress.Status)
                            {
                                case Google.Apis.Download.DownloadStatus.Downloading:
                                    {
                                        Debug.Log("Downloading");
                                        break;
                                    }
                                case Google.Apis.Download.DownloadStatus.Completed:
                                    {
                                        Debug.Log("Download completed");
                                        break;
                                    }
                                case Google.Apis.Download.DownloadStatus.Failed:
                                    {
                                        Debug.Log("Download failed");
                                        break;
                                    }
                            }
                        };

                        file_request.Download(file_stream);

                        byte[] bytes = new byte[file_stream.Length];
                        int bytes_to_read = (int)file_stream.Length;

                        file_stream.Write(bytes, 0, bytes_to_read);

                        file_stream.Close();

                    }

                }
            }

        }

        public void ReadDialogue()
        {
            StreamReader stream_reader = new StreamReader(dialogue_file);

            if (dialogue_list == null)
            {
                dialogue_list = new List<string>();
            }

            while (!stream_reader.EndOfStream)
            {
                dialogue_list.Add(stream_reader.ReadLine());                
            }
            dialogue_list.RemoveAll(s => s == "");

            stream_reader.Close();

            Dialogue.ImportMessages(dialogue_list);
            dialogue_list.Clear();
        }
    }
}
