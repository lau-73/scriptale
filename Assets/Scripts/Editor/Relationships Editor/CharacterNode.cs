﻿using System;
using UnityEngine;


public class CharacterNode
{

    public int id;
    public string CharName;
    public Rect GUIRect;
    
    public bool selected;
    public bool dragged;

    public GUIStyle style;
    public GUIStyle defaultNodeStyle;
    public GUIStyle selectedNodeStyle;
    
    public Action<CharacterNode> OnClickInRelationship;
    public Action<CharacterNode> OnClickOutRelationship;

    public CharacterNode()
    { }

    public CharacterNode(int char_id, string name, Vector2 pos, float w, float h, GUIStyle node_style, Action<CharacterNode> OnClickInPoint, Action<CharacterNode> OnClickOutPoint, GUIStyle selected_style)
    {
        id = char_id;
        GUIRect = new Rect(pos.x, pos.y, w, h);
        style = node_style;
        defaultNodeStyle = node_style;
        selectedNodeStyle = selected_style;

        OnClickInRelationship = OnClickInPoint;
        OnClickOutRelationship = OnClickOutPoint;

        CharName = name;
        
    }

    public void Draw()
    {
        GUI.Box(GUIRect, CharName, style);
        
    }

    public void Drag(Vector2 move)
    {
        GUIRect.position += move;
    }

    public bool ProcessEvents(Event ev, ref bool connecting)
    {
        switch (ev.type)
        {
            case EventType.MouseDown:
                if (ev.button == 0)
                {

                    if (GUIRect.Contains(ev.mousePosition) && connecting)
                    {
                        dragged = true;
                        selected = true;
                        style = selectedNodeStyle;

                        OnClickOutRelationship(this);
                        connecting = false;
                        GUI.changed = true;
                    }
                    else if (GUIRect.Contains(ev.mousePosition) && !connecting)
                    {
                        dragged = true;
                        selected = true;
                        style = selectedNodeStyle;
                        
                        GUI.changed = true;
                    }
                    else
                    {
                        GUI.changed = true;

                        selected = false;
                        style = defaultNodeStyle;
                    }
                }
                
                break;

            case EventType.MouseUp:
                dragged = false;
                break;

            case EventType.MouseDrag:
                if (ev.button == 0 && dragged)
                {
                    Drag(ev.delta);
                    ev.Use();
                    return true;
                }
                break;

            case EventType.KeyDown:
                if(ev.keyCode == KeyCode.Return && selected && !connecting)
                {
                    OnClickInRelationship(this);
                    connecting = true;
                }
                break;
                
        }
        return false;
    }

}
