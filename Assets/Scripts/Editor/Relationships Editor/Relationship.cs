﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Relationship
{
    public CharacterNode inPoint;
    public CharacterNode outPoint;
    public List<sTaleUtils.Effect> influence;

    public Action<Relationship> OnRemoveRelationship;

    public Relationship() {
        influence = new List<sTaleUtils.Effect>();
    }
    public Relationship(CharacterNode in_point, CharacterNode out_point, Action<Relationship> RemoveRelationship)
    {
        inPoint = in_point;
        outPoint = out_point;
        OnRemoveRelationship = RemoveRelationship;
        influence = new List<sTaleUtils.Effect>();
    }

    public void Draw()
    {
        Handles.DrawBezier(inPoint.GUIRect.center, outPoint.GUIRect.center, inPoint.GUIRect.center + Vector2.down * 50f, outPoint.GUIRect.center - Vector2.down * 50f, Color.white, null, 2f);

        if (Handles.Button((inPoint.GUIRect.center + outPoint.GUIRect.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap))
            OnRemoveRelationship?.Invoke(this);
    }

    public void DrawRelationship()
    {
        for (int em = 0; em < influence.Count; ++em)
        {
            GUILayout.BeginHorizontal();

            influence[em].emotion = (sTaleUtils.EmotionType)EditorGUILayout.EnumPopup(influence[em].emotion, GUILayout.Width(75));
            influence[em].level = EditorGUILayout.FloatField(influence[em].level, GUILayout.Width(75));
            
            GUILayout.EndHorizontal();
        }
        
        if (GUILayout.Button("Add influence " + inPoint.CharName + " -> " + outPoint.CharName, GUILayout.Width(200)))
        {
            influence.Add(new sTaleUtils.Effect(sTaleUtils.EmotionType.None, 0.0f));

        }
        
    }

    public void Load(List<sTaleUtils.Effect> influence_list)
    {
        influence.Clear();

        influence = influence_list;
    }

}