﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.IO;

public class sTaleRelationshipsEditor : EditorWindow
{

    private static string pathtoload;
    private static List<CharacterNode> characters;
    private static List<Relationship> connections;
    
    private static GUIStyle stateStyle;
    private static GUIStyle selectedNodeStyle;

    private static CharacterNode selectedInNode;
    private static CharacterNode selectedOutNode;
    private bool connecting = false;

    private Vector2 dragCanvas;
    private Vector2 offset;

    private Rect menuBar;
    private Vector2 scrollPosition = Vector2.zero;

    public delegate void OnPathChanged();

    public static event OnPathChanged pathChanged;

    static sTaleRelationshipsEditor()
    {
        pathtoload = "";
        pathChanged += Load;

        // Style of state's node
        stateStyle = new GUIStyle();
        selectedNodeStyle = new GUIStyle();
    }

    ~sTaleRelationshipsEditor()
    {
        pathChanged -= Load;
    }


    [MenuItem("Window/ScripTale Relationships Editor")]
    static public void OpenEditor()
    {
        EditorWindow window = EditorWindow.GetWindow<sTaleRelationshipsEditor>("ScripTale Relationships Editor");
        window.titleContent = new GUIContent("ScripTale Relationships Editor");
    }

    static public void SetRelationshipsPath(string path)
    {
        pathtoload = path;
        pathChanged();
    }

    private void OnEnable()
    {

        stateStyle.normal.background = EditorGUIUtility.Load("builtin skins/lightskin/images/node1.png") as Texture2D;
        stateStyle.border = new RectOffset(12, 12, 12, 12);
        stateStyle.alignment = TextAnchor.MiddleCenter;

        selectedNodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/lightskin/images/node1 on.png") as Texture2D;
        selectedNodeStyle.border = new RectOffset(12, 12, 12, 12);
        selectedNodeStyle.alignment = TextAnchor.MiddleCenter;

        if (characters == null)
        {
            characters = new List<CharacterNode>();
            CreateCharacters();
        }
    }

    private void CreateCharacters()
    {
        sTaleCharacter[] characters_scene = FindObjectsOfType<sTaleCharacter>();

        for(int i = 0; i < characters_scene.Length; ++i)
        {
            characters.Add(new CharacterNode(characters_scene[i].identifier, characters_scene[i].name, new Vector2(300, (i * 100) + 100), 200, 60, stateStyle, OnClickInNode, OnClickOutNode, selectedNodeStyle));
        }

    }
    private void OnGUI()
    {

        // Drawing Grids
        DrawGrid(20, Color.gray, 0.2f);
        DrawGrid(100, Color.gray, 0.4f);

        DrawConnections();
        
        DrawCharacters();
        
        DrawConnectionLine(Event.current);
        
        DrawMenuBar();

        scrollPosition = GUI.BeginScrollView(new Rect(10, 30, 300, position.height), scrollPosition, new Rect(0, 0, 300, position.height));
        
        DrawRelationships();
        GUI.EndScrollView();


        ProcessCharactersEvents(Event.current);
        ProcessEvents(Event.current);

        if (GUI.changed)
        {
            Repaint();
            //And update story
        }
    }

    // Draw functions

    private void DrawCharacters()
    {
        if (characters != null)
        {
            for (int i = 0; i < characters.Count; ++i)
            {
                characters[i].Draw();
            }
        }
    }

    private void DrawConnections()
    {
        if (connections != null)
        {
            for (int i = 0; i < connections.Count; i++)
            {
                connections[i].Draw();
            }
        }
    }

    private void DrawConnectionLine(Event ev)
    {
        if (selectedInNode != null && selectedOutNode == null)
        {
            Handles.DrawBezier(selectedInNode.GUIRect.center, ev.mousePosition, selectedInNode.GUIRect.center + Vector2.down * 50f, ev.mousePosition - Vector2.down * 50f, Color.red, null, 2f);
            GUI.changed = true;
        }

        if (selectedOutNode != null && selectedInNode == null)
        {
            Handles.DrawBezier(selectedOutNode.GUIRect.center, ev.mousePosition, selectedOutNode.GUIRect.center - Vector2.down * 50f, ev.mousePosition + Vector2.down * 50f, Color.red, null, 2f);
            GUI.changed = true;
        }
    }

    private void DrawGrid(float spacing, Color color, float alpha)
    {
        int w_division = Mathf.CeilToInt(position.width / spacing);
        int h_division = Mathf.CeilToInt(position.height / spacing);

        Handles.BeginGUI();
        Handles.color = new Color(color.r, color.g, color.b, alpha);

        offset += dragCanvas * 0.5f;
        Vector3 spatial_offset = new Vector3(offset.x % spacing, offset.y % spacing, 0);

        for (int w = 0; w < w_division; ++w)
        {
            Handles.DrawLine(new Vector3(spacing * w, -spacing, 0) + spatial_offset, new Vector3(spacing * w, position.height, 0f) + spatial_offset);
        }

        for (int h = 0; h < h_division; ++h)
        {
            Handles.DrawLine(new Vector3(-spacing, spacing * h, 0) + spatial_offset, new Vector3(position.width, spacing * h, 0f) + spatial_offset);
        }

        Handles.color = Color.white;
        Handles.EndGUI();
    }

    private void DrawMenuBar()
    {
        menuBar = new Rect(0, 0, position.width, 30);
        
        GUILayout.BeginArea(menuBar, EditorStyles.toolbar);
        
        if (GUILayout.Button(new GUIContent("Save"), EditorStyles.toolbarButton, GUILayout.Width(35)))
        {
            Save();
        }
        
        GUILayout.EndArea();
    }

    private void DrawRelationships()
    {
        if (connections != null)
        {
            for (int i = 0; i < connections.Count; i++)
            {
                connections[i].DrawRelationship();
            }
        }
    }

    private void Save()
    {
        // CALCULATE SIZE
        //states count
        byte[] char_count = new byte[4];
        char_count = BitConverter.GetBytes(characters.Count);

        if (BitConverter.IsLittleEndian)
            Array.Reverse(char_count);

        int size = char_count.Length;

        for (int i = 0; i < characters.Count; ++i)
        {
            size += 8; //id + name length
            size += characters[i].CharName.Length; //name length
            size += (4 * 4); //rect
            
        }

        // Make sure connections is not set to null
        if (connections == null)
            connections = new List<Relationship>();

        // connections count
        byte[] connections_count = new byte[4];
        connections_count = BitConverter.GetBytes(connections.Count);

        if (BitConverter.IsLittleEndian)
            Array.Reverse(connections_count);

        size += connections_count.Length;

        for (int i = 0; i < connections.Count; ++i)
        {
            size += 8; //inpoint state id + outpoint state id

            size += 4; //number of influences

            for(int inf = 0; inf < connections[i].influence.Count; ++inf)
            {
                size += 8; //Emotion and level
            }
        }

        //SERIALIZE IN AN OWN FORMAT TO BUFFER
        byte[] buffer = new byte[size];

        // Order 1 states_count
        int cursor = 0;
        Buffer.BlockCopy(char_count, 0, buffer, cursor, sizeof(byte) * 4);
        cursor += (sizeof(byte) * 4); // 1 * 4

        for (int i = 0; i < characters.Count; ++i)
        {
            // Order 2 id
            byte[] id = new byte[4];

            id = BitConverter.GetBytes(characters[i].id);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(id);

            Buffer.BlockCopy(id, 0, buffer, cursor, id.Length);
            cursor += (sizeof(byte) * 4);

            // Order 3 name length
            byte[] name_length = new byte[4];

            name_length = BitConverter.GetBytes(characters[i].CharName.Length);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(name_length);

            Buffer.BlockCopy(name_length, 0, buffer, cursor, name_length.Length);
            cursor += (sizeof(byte) * 4);

            // Order 4 name
            byte[] name = System.Text.Encoding.ASCII.GetBytes(characters[i].CharName);

            Buffer.BlockCopy(name, 0, buffer, cursor, name.Length);
            cursor += name.Length;

            // Order 5, 6, 7, 8 rect

            byte[] rect_x = new byte[4];

            rect_x = BitConverter.GetBytes(characters[i].GUIRect.x);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(rect_x);

            Buffer.BlockCopy(rect_x, 0, buffer, cursor, rect_x.Length);
            cursor += (sizeof(byte) * 4);

            byte[] rect_y = new byte[4];

            rect_y = BitConverter.GetBytes(characters[i].GUIRect.y);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(rect_y);

            Buffer.BlockCopy(rect_y, 0, buffer, cursor, rect_y.Length);
            cursor += (sizeof(byte) * 4);

            byte[] rect_w = new byte[4];

            rect_w = BitConverter.GetBytes(characters[i].GUIRect.width);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(rect_w);

            Buffer.BlockCopy(rect_w, 0, buffer, cursor, rect_w.Length);
            cursor += (sizeof(byte) * 4);

            byte[] rect_h = new byte[4];

            rect_h = BitConverter.GetBytes(characters[i].GUIRect.height);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(rect_h);

            Buffer.BlockCopy(rect_h, 0, buffer, cursor, rect_h.Length);
            cursor += (sizeof(byte) * 4);
        }

        // Order 15 Connections count

        Buffer.BlockCopy(connections_count, 0, buffer, cursor, connections_count.Length);
        cursor += connections_count.Length;

        for (int i = 0; i < connections.Count; ++i)
        {
            // Order 16 in_id
            byte[] in_id = new byte[4];
            in_id = BitConverter.GetBytes(connections[i].inPoint.id);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(in_id);

            Buffer.BlockCopy(in_id, 0, buffer, cursor, in_id.Length);
            cursor += sizeof(byte) * 4;

            // Order 17 out_id
            byte[] out_id = new byte[4];
            out_id = BitConverter.GetBytes(connections[i].outPoint.id);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(out_id);

            Buffer.BlockCopy(out_id, 0, buffer, cursor, out_id.Length);
            cursor += sizeof(byte) * 4;

            byte[] inf_c = new byte[4];
            inf_c = BitConverter.GetBytes(connections[i].influence.Count);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(inf_c);

            Buffer.BlockCopy(inf_c, 0, buffer, cursor, inf_c.Length);
            cursor += sizeof(byte) * 4;

            for (int inf = 0; inf < connections[i].influence.Count; ++inf)
            {
                byte[] emotion_type = new byte[4];
                emotion_type = BitConverter.GetBytes((int)connections[i].influence[inf].emotion);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(emotion_type);

                Buffer.BlockCopy(emotion_type, 0, buffer, cursor, emotion_type.Length);
                cursor += emotion_type.Length;

                byte[] emotion_lev = new byte[4];
                emotion_lev = BitConverter.GetBytes(connections[i].influence[inf].level);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(emotion_lev);

                Buffer.BlockCopy(emotion_lev, 0, buffer, cursor, emotion_lev.Length);
                cursor += emotion_lev.Length;
            }

        }

        string pathtosave = EditorUtility.SaveFilePanel("Relationships", Application.streamingAssetsPath, "Relationships", "rstale");
        if (pathtosave.Length != 0)
        {
            StreamWriter states_writer = new StreamWriter(pathtosave);
            states_writer.BaseStream.Write(buffer, 0, size);
            states_writer.Flush();
            states_writer.Close();

        }
    }

    public static void Load()
    {
        byte[] data;
        using (StreamReader sr = new StreamReader(pathtoload))
        {
            using (MemoryStream ms = new MemoryStream())
            {
                sr.BaseStream.CopyTo(ms);
                data = ms.ToArray();
            }
        }

        int cursor = 0;

        byte[] char_c = new byte[4];
        Buffer.BlockCopy(data, cursor, char_c, 0, 4);

        if (BitConverter.IsLittleEndian)
            Array.Reverse(char_c);

        int char_count = BitConverter.ToInt32(char_c, 0);
        cursor += sizeof(byte) * 4;

        if (characters != null)
        {
            characters.Clear();
        }
        else
        {
            characters = new List<CharacterNode>();
        }

        for (int i = 0; i < char_count; ++i)
        {

            byte[] char_id = new byte[4];
            Buffer.BlockCopy(data, cursor, char_id, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(char_id);

            int id = BitConverter.ToInt32(char_id, 0);
            cursor += sizeof(byte) * 4;

            byte[] name_length = new byte[4];
            Buffer.BlockCopy(data, cursor, name_length, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(name_length);

            int char_name_len = BitConverter.ToInt32(name_length, 0);
            cursor += sizeof(byte) * 4;

            byte[] name = new byte[char_name_len];
            Buffer.BlockCopy(data, cursor, name, 0, char_name_len);

            string char_name = System.Text.Encoding.ASCII.GetString(name);
            cursor += char_name_len;

            byte[] rectx = new byte[4];
            byte[] recty = new byte[4];
            byte[] rectw = new byte[4];
            byte[] recth = new byte[4];

            Buffer.BlockCopy(data, cursor, rectx, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(rectx);

            float rec_x = BitConverter.ToSingle(rectx, 0);
            cursor += sizeof(byte) * 4;

            Buffer.BlockCopy(data, cursor, recty, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(recty);

            float rec_y = BitConverter.ToSingle(recty, 0);
            cursor += sizeof(byte) * 4;


            Buffer.BlockCopy(data, cursor, rectw, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(rectw);

            float rec_w = BitConverter.ToSingle(rectw, 0);
            cursor += sizeof(byte) * 4;

            Buffer.BlockCopy(data, cursor, recth, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(recth);

            float rec_h = BitConverter.ToSingle(recth, 0);
            cursor += sizeof(byte) * 4;

            CharacterNode charac = new CharacterNode(
                id, char_name,
                new Vector2(rec_x, rec_y),
                rec_w, rec_h,
                stateStyle,
                OnClickInNode, OnClickOutNode,
                selectedNodeStyle
                );

            characters.Add(charac);
        }

        if (connections != null)
        {
            connections.Clear();
        }
        else
        {
            connections = new List<Relationship>();
        }


        byte[] connections_c = new byte[4];
        Buffer.BlockCopy(data, cursor, connections_c, 0, 4);

        if (BitConverter.IsLittleEndian)
            Array.Reverse(connections_c);

        int conn_count = BitConverter.ToInt32(connections_c, 0);
        cursor += sizeof(byte) * 4;


        for (int i = 0; i < conn_count; ++i)
        {

            byte[] instate = new byte[4];
            Buffer.BlockCopy(data, cursor, instate, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(instate);

            int in_id = BitConverter.ToInt32(instate, 0);
            cursor += sizeof(byte) * 4;

            byte[] outstate = new byte[4];
            Buffer.BlockCopy(data, cursor, outstate, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(outstate);

            int out_id = BitConverter.ToInt32(outstate, 0);
            cursor += sizeof(byte) * 4;
            
            connections.Add(new Relationship(
                characters.Find(c => c.id == in_id),
                characters.Find(c => c.id == out_id),
                OnClickRemoveConnection
                ));

            byte[] inf_c = new byte[4];
            Buffer.BlockCopy(data, cursor, inf_c, 0, inf_c.Length);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(inf_c);

            int influence_count = BitConverter.ToInt32(inf_c, 0);
            cursor += sizeof(byte) * 4;

            List<sTaleUtils.Effect> influence = new List<sTaleUtils.Effect>();

            for (int inf = 0; inf < influence_count; ++inf)
            {
                byte[] emotion_type = new byte[4];
                Buffer.BlockCopy(data, cursor, emotion_type, 0, emotion_type.Length);
                
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(emotion_type);

                int em_type = BitConverter.ToInt32(emotion_type, 0);
                cursor += emotion_type.Length;

                byte[] emotion_lev = new byte[4];
                Buffer.BlockCopy(data, cursor, emotion_lev, 0, emotion_lev.Length);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(emotion_lev);

                float emotion_level = BitConverter.ToSingle(emotion_lev, 0);
                cursor += emotion_lev.Length;

                influence.Add(new sTaleUtils.Effect((sTaleUtils.EmotionType)em_type, emotion_level));
            }

            connections[i].Load(influence);
        }
        
    }

    // Process events

    private void ProcessEvents(Event ev)
    {
        dragCanvas = Vector2.zero;
        if (ev.type == EventType.MouseDown)
        {
            switch (ev.button)
            {
                case 0:
                    GUI.FocusControl(null);
                    break;
                case 1: //Right button down
                    ShowOptions(ev.mousePosition);
                    break;

            }
        }
        if (ev.type == EventType.MouseDrag)
        {
            if (ev.button == 0)
            {
                OnDrag(ev.delta);
            }
        }

    }

    private void ProcessCharactersEvents(Event ev)
    {
        if (characters != null)
        {
            for (int i = characters.Count - 1; i >= 0; i--)
            {
                bool guiChanged = characters[i].ProcessEvents(ev, ref connecting);

                if (guiChanged)
                {
                    GUI.changed = true;
                }
            }
        }
    }

    // Callback functions

    private void ShowOptions(Vector2 mousePosition)
    {
        GenericMenu options_menu = new GenericMenu();
        options_menu.AddItem(new GUIContent("Clear all relationships"), false, () => OnClickClearAllRelationships());
        options_menu.ShowAsContext();
    }

    private void OnClickClearAllRelationships()
    {
        if (connections != null)
        {
            if (connections.Count > 0)
            {
                for (int i = connections.Count - 1; i >= 0; --i)
                {
                    connections[i].OnRemoveRelationship?.Invoke(connections[i]);
                }
            }
        }
    }

    private void OnDrag(Vector2 delta)
    {
        dragCanvas = delta;

        if (characters != null)
        {
            for (int i = 0; i < characters.Count; i++)
            {
                characters[i].Drag(delta);
            }
        }

        GUI.changed = true;
    }

    private static void OnClickInNode(CharacterNode inNode)
    {
        selectedInNode = inNode;

        if (selectedOutNode != null)
        {
            if (selectedOutNode != selectedInNode)
            {
                CreateConnection();
                ClearConnectionSelection();
            }
            else
            {
                ClearConnectionSelection();
            }
        }
    }

    private static void OnClickOutNode(CharacterNode outPoint)
    {
        selectedOutNode = outPoint;

        if (selectedInNode != null)
        {
            if (selectedOutNode != selectedInNode)
            {
                CreateConnection();
                ClearConnectionSelection();
            }
            else
            {
                ClearConnectionSelection();
            }
        }
    }

    private static void OnClickRemoveConnection(Relationship connection)
    {
        connections.Remove(connection);
    }

    private static void CreateConnection()
    {
        if (connections == null)
        {
            connections = new List<Relationship>();
        }

        if (connections.Find(c => c.inPoint.id == selectedInNode.id && c.outPoint.id == selectedOutNode.id) == null && connections.Find(c => c.outPoint.id == selectedInNode.id && c.inPoint.id == selectedOutNode.id) == null)
            connections.Add(new Relationship(selectedInNode, selectedOutNode, OnClickRemoveConnection));
        else
            ClearConnectionSelection();
    }

    private static void ClearConnectionSelection()
    {
        selectedInNode = null;
        selectedOutNode = null;
    }

}
