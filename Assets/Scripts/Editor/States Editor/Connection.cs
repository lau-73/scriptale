﻿using System;
using UnityEditor;
using UnityEngine;

public class Connection
{
    public ConnectionPoint inPoint;
    public ConnectionPoint outPoint;

    public Action<Connection> OnRemoveConnection;

    public Connection() { }
    public Connection(ConnectionPoint in_point, ConnectionPoint out_point, Action<Connection> RemoveConnection)
    {
        inPoint = in_point;
        outPoint = out_point;
        OnRemoveConnection = RemoveConnection;
    }

    public void Draw()
    {
        Handles.DrawBezier(inPoint.pointRect.center, outPoint.pointRect.center, inPoint.pointRect.center + Vector2.down * 50f, outPoint.pointRect.center - Vector2.down * 50f, Color.white, null, 2f);

        if (Handles.Button((inPoint.pointRect.center + outPoint.pointRect.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap))
            OnRemoveConnection?.Invoke(this);
    }
}