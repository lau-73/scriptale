﻿using System;
using UnityEditor;
using UnityEngine;

public enum ConnectionType { In, Out }

public class ConnectionPoint {

    public Rect pointRect;
    public ConnectionType type;
    public GUIStyle style;

    public State state;
    public Action<ConnectionPoint> OnClickConnectionPoint;

    public ConnectionPoint() { }
    public ConnectionPoint(State state, ConnectionType type, Action<ConnectionPoint> OnClickConnectionPoint)
    {
        this.type = type;
        
        this.state = state;

        style = new GUIStyle();
        style.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left.png") as Texture2D;
        style.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left on.png") as Texture2D;
        style.border = new RectOffset(4, 4, 12, 12);
        
        this.OnClickConnectionPoint = OnClickConnectionPoint;

        pointRect = new Rect(0, 0, 20f, 10f);
    }

    public void Draw()
    {
        switch(type)
        {
            case ConnectionType.In:
                pointRect.y = state.GUIRect.y - pointRect.height / 2;
                break;

            case ConnectionType.Out:
                pointRect.y = state.GUIRect.y + state.GUIRect.height - pointRect.height / 2;
                break;
        }


        pointRect.x = state.GUIRect.x + (state.GUIRect.width / 2) - pointRect.width / 2;
        
        if (GUI.Button(pointRect, "", style))
        {
            if (OnClickConnectionPoint != null)
            {
                OnClickConnectionPoint(this);
            }
        }
    }
}
