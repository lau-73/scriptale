﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class ConditionsDictionary<Type>
{
    public string gameObjectName;
    public List<Type> conditions;

    public ConditionsDictionary (string name)
    {
        gameObjectName = name;
        conditions = new List<Type>();
    }

    public ConditionsDictionary()
    {   
        conditions = new List<Type>();
    }
}

public class State {

    public int id;
    public string stateName;
    public Rect GUIRect;

    public List<GameObject> objectRequirements;

    public int conditionsSet = 0;
    public List<ConditionsDictionary<sTaleUtils.Effect>> returnEffect;

    public ConnectionPoint inPoint;
    public ConnectionPoint outPoint;

    public bool selected;
    public bool dragged;

    public GUIStyle style;
    public GUIStyle defaultNodeStyle;
    public GUIStyle selectedNodeStyle;

    public Action<State> OnRemoveState;
    private Vector2 scrollPos;
    
    public State()
    { }

    public State(int state_id, string name, Vector2 pos, float w, float h, GUIStyle node_style, Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, GUIStyle selected_style, Action<State> OnClickRemoveState)
    {
        id = state_id;
        GUIRect = new Rect(pos.x, pos.y, w, h);
        style = node_style;
        inPoint = new ConnectionPoint(this, ConnectionType.In, OnClickInPoint);
        outPoint = new ConnectionPoint(this, ConnectionType.Out, OnClickOutPoint);
        defaultNodeStyle = node_style;
        selectedNodeStyle = selected_style;

        stateName = name;

        OnRemoveState = OnClickRemoveState;
        scrollPos = new Vector2(0, 0);
    }

    public void Draw()
    {
        inPoint.Draw();
        outPoint.Draw();


        GUILayout.BeginArea(GUIRect, style);
        
        EditorGUILayout.BeginVertical(GUILayout.Width(GUIRect.width), GUILayout.Height(GUIRect.height));
        GUILayout.FlexibleSpace();

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        stateName = EditorGUILayout.TextArea(stateName);

        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();

        scrollPos = GUILayout.BeginScrollView(scrollPos, GUILayout.Width(GUIRect.width), GUILayout.Height(GUIRect.height - 40));

        DrawConditions();

        GUILayout.EndScrollView();

        GUILayout.FlexibleSpace();
        EditorGUILayout.EndVertical();

        GUILayout.EndArea();
    }
    
    public void Drag(Vector2 move)
    {
        GUIRect.position += move;
    }

    public bool ProcessEvents(Event ev)
    {
        switch (ev.type)
        {
            case EventType.MouseDown:
                if (ev.button == 0)
                {
                    if (GUIRect.Contains(ev.mousePosition))
                    {
                        dragged = true;
                        selected = true;
                        style = selectedNodeStyle;
                        GUI.changed = true;
                    }
                    else
                    {
                        GUI.changed = true;

                        selected = false;
                        style = defaultNodeStyle;
                    }
                }

                if (ev.button == 1 && selected && GUIRect.Contains(ev.mousePosition))
                {
                    ShowOptions();
                    ev.Use();
                }
                break;

            case EventType.MouseUp:
                dragged = false;
                break;

            case EventType.MouseDrag:
                if (ev.button == 0 && dragged)
                {
                    Drag(ev.delta);
                    ev.Use();
                    return true;
                }
                break;


            case EventType.KeyDown:
                
                if (ev.keyCode == (KeyCode.Delete) && selected)
                {
                    OnClickRemoveState();
                    ev.Use();
                    return true;
                }
                break;
        }
        return false;
    }

    private void ShowOptions()
    {
        GenericMenu genericMenu = new GenericMenu();
        genericMenu.AddItem(new GUIContent("Add condition"), false, OnClickAddCondition);
        genericMenu.AddItem(new GUIContent("Remove state"), false, OnClickRemoveState);
        genericMenu.ShowAsContext();
    }

    private void OnClickRemoveState()
    {
        OnRemoveState?.Invoke(this);
        /* Same as:
        if (OnRemoveState != null)
        {
            OnRemoveState(this);
        }
        */
    }

    private void OnClickAddCondition()
    {
        ++conditionsSet;

        if(objectRequirements == null)
        {
            objectRequirements = new List<GameObject>();
        }
        objectRequirements.Add(null);

    }

    private void DrawConditions()
    {
        for(int i = 0; i < conditionsSet; ++i)
        {
            
            EditorGUI.BeginChangeCheck();

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            objectRequirements[i] = (GameObject)EditorGUILayout.ObjectField(objectRequirements[i], typeof(GameObject), true, GUILayout.Width(GUIRect.width - 50), GUILayout.Height(20));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck())
            {
                if (objectRequirements[i] != null)
                {
                    if (objectRequirements[i].GetComponent<sTaleCharacter>())
                    {
                        
                        Debug.Log(objectRequirements[i].GetComponent<sTaleCharacter>().identifier);

                        if (returnEffect == null)
                        {
                            returnEffect = new List<ConditionsDictionary<sTaleUtils.Effect>>();
                            returnEffect.Add(new ConditionsDictionary<sTaleUtils.Effect>(objectRequirements[i].name));

                        }

                    }

                }
            }

            if (objectRequirements[i] != null)
            {
                if (objectRequirements[i].GetComponent<sTaleCharacter>())
                {
                    for(int em = 0; em < returnEffect[i].conditions.Count; ++em)
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.FlexibleSpace();
                        
                        
                        returnEffect[i].conditions[em].emotion = (sTaleUtils.EmotionType)EditorGUILayout.EnumPopup(returnEffect[i].conditions[em].emotion, GUILayout.Width(75));
                        
                        if(em < returnEffect[i].conditions.Count)
                            returnEffect[i].conditions[em].level = EditorGUILayout.FloatField(returnEffect[i].conditions[em].level, GUILayout.Width(75));

                        GUILayout.FlexibleSpace();
                        GUILayout.EndHorizontal();
                    }

                    GUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();

                    if (GUILayout.Button("Add emotion", GUILayout.Width(GUIRect.width - 50)))
                    {
                        returnEffect[i].conditions.Add(new sTaleUtils.Effect(sTaleUtils.EmotionType.None, 0.0f));
                        
                    }

                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();
                }
                
            }
        }
    }

    public void Load(string go_name, List<sTaleUtils.Effect> effects)
    {
        if (objectRequirements == null)
            objectRequirements = new List<GameObject>();

        objectRequirements.Add(GameObject.Find(go_name));

        if (returnEffect == null)
            returnEffect = new List<ConditionsDictionary<sTaleUtils.Effect>>();

        ConditionsDictionary<sTaleUtils.Effect> obj_effects = new ConditionsDictionary<sTaleUtils.Effect>();

        obj_effects.gameObjectName = go_name;
        obj_effects.conditions = effects;

        returnEffect.Add(obj_effects);

        conditionsSet++;
    }

}
