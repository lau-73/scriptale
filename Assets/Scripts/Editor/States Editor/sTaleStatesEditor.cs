﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.IO;

public class sTaleStatesEditor : EditorWindow {

    private static string pathtoload;
    private static List<State> states;
    private static List<Connection> connections;

    // GUI style
    private static GUIStyle stateStyle;
    private static GUIStyle selectedNodeStyle;
                        
    private static ConnectionPoint selectedInPoint;
    private static ConnectionPoint selectedOutPoint;

    private Vector2 dragCanvas;
    private Vector2 offset;

    private Rect menuBar;

    public delegate void OnPathChanged();

    public static event OnPathChanged pathChanged;

    static sTaleStatesEditor()
    {
        pathtoload = "";
        pathChanged += Load;
        
        // Style of state's node
        stateStyle = new GUIStyle();
        selectedNodeStyle = new GUIStyle();
    }

    ~sTaleStatesEditor()
    {
        pathChanged -= Load;
    }


    [MenuItem("Window/ScripTale States Editor")]
    static public void OpenEditor()
    {
        EditorWindow window = EditorWindow.GetWindow<sTaleStatesEditor>("ScripTale States Editor");
        window.titleContent = new GUIContent("ScripTale States Editor");
    }

    static public void SetStatesPath(string path)
    {
        pathtoload = path;
        pathChanged();
    }

    private void OnEnable()
    {

        stateStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
        stateStyle.border = new RectOffset(12, 12, 12, 12);
        stateStyle.alignment = TextAnchor.UpperCenter;

        selectedNodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1 on.png") as Texture2D;
        selectedNodeStyle.border = new RectOffset(12, 12, 12, 12);
        selectedNodeStyle.alignment = TextAnchor.UpperCenter;
               

    }

    private void OnGUI()
    {
        
        // Drawing Grids
        DrawGrid(20, Color.gray, 0.2f);
        DrawGrid(100, Color.gray, 0.4f);

        // Drawing States and connections
        DrawStates();
        DrawConnections();

        DrawConnectionLine(Event.current);

        DrawMenuBar();

        ProcessStatesEvents(Event.current);
        ProcessEvents(Event.current);

        if (GUI.changed)
        {
            Repaint();
            //And update story
        }
    }

    // Draw functions

    private void DrawStates()
    {
        if (states != null)
        {
            for (int i = 0; i < states.Count; ++i)
            {
                states[i].Draw();
            }
        }
    }

    private void DrawConnections()
    {
        if (connections != null)
        {
            for (int i = 0; i < connections.Count; i++)
            {
                connections[i].Draw();
            }
        }
    }
    
    private void DrawConnectionLine(Event ev)
    {
        if (selectedInPoint != null && selectedOutPoint == null)
        {
            Handles.DrawBezier(selectedInPoint.pointRect.center, ev.mousePosition, selectedInPoint.pointRect.center + Vector2.down * 50f, ev.mousePosition - Vector2.down * 50f, Color.red, null, 2f);
            GUI.changed = true;
        }

        if (selectedOutPoint != null && selectedInPoint == null)
        {
            Handles.DrawBezier(selectedOutPoint.pointRect.center, ev.mousePosition, selectedOutPoint.pointRect.center - Vector2.down * 50f, ev.mousePosition + Vector2.down * 50f, Color.red, null, 2f);
            GUI.changed = true;
        }
    }

    private void DrawGrid(float spacing, Color color, float alpha)
    {
        int w_division = Mathf.CeilToInt(position.width / spacing);
        int h_division = Mathf.CeilToInt(position.height / spacing);

        Handles.BeginGUI();
        Handles.color = new Color(color.r, color.g, color.b, alpha);

        offset += dragCanvas * 0.5f;
        Vector3 spatial_offset = new Vector3(offset.x % spacing, offset.y % spacing, 0);

        for (int w = 0; w < w_division; ++w)
        {
            Handles.DrawLine(new Vector3(spacing * w, -spacing, 0) + spatial_offset, new Vector3(spacing * w, position.height, 0f) + spatial_offset);
        }

        for (int h = 0; h < h_division; ++h)
        {
            Handles.DrawLine(new Vector3(-spacing, spacing * h, 0) + spatial_offset, new Vector3(position.width, spacing * h, 0f) + spatial_offset);
        }

        Handles.color = Color.white;
        Handles.EndGUI();
    }

    private void DrawMenuBar()
    {
        menuBar = new Rect(0, 0, position.width, 30);

        GUILayout.BeginArea(menuBar, EditorStyles.toolbar);
        GUILayout.BeginHorizontal();

        if (GUILayout.Button(new GUIContent("Save"), EditorStyles.toolbarButton, GUILayout.Width(35)))
        {
            Save();
        }

        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    private void Save()
    {
        // CALCULATE SIZE
        //states count
        byte[] states_count = new byte[4];
        states_count = BitConverter.GetBytes(states.Count);

        if (BitConverter.IsLittleEndian)
            Array.Reverse(states_count);

        int size = states_count.Length;

        for (int i = 0; i < states.Count; ++i)
        {
            size += 8; //id + name length
            size += states[i].stateName.Length; //name length
            size += (4 * 4); //rect

            size += 4; //number of conditions set

            if (states[i].conditionsSet > 0)
            {
                for (int cond = 0; cond < states[i].conditionsSet; ++cond)
                {
                    //Object name length + number of emotions + object name

                    size += (8 + states[i].returnEffect[cond].gameObjectName.Length);

                    for (int emotions = 0; emotions < states[i].returnEffect[cond].conditions.Count; ++emotions)
                    {
                        //Emotion type + emotion level
                        size += 8;
                    }

                }
            }

        }

        // Make sure connections is not set to null
        if (connections == null)
            connections = new List<Connection>();

        // connections count
        byte[] connections_count = new byte[4];
        connections_count = BitConverter.GetBytes(connections.Count);

        if (BitConverter.IsLittleEndian)
            Array.Reverse(connections_count);

        size += connections_count.Length;
        
        for (int i = 0; i < connections.Count; ++i)
        {
            size += 8; //inpoint state id + outpoint state id
        }

        //SERIALIZE IN AN OWN FORMAT TO BUFFER
        byte[] buffer = new byte[size];

        // Order 1 states_count
        int cursor = 0;
        Buffer.BlockCopy(states_count, 0, buffer, cursor, sizeof(byte) * 4);
        cursor += (sizeof(byte) * 4); // 1 * 4

        for (int i = 0; i < states.Count; ++i)
        {
            // Order 2 id
            byte[] id = new byte[4];

            id = BitConverter.GetBytes(states[i].id);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(id);
            
            Buffer.BlockCopy(id, 0, buffer, cursor, id.Length);
            cursor += (sizeof(byte) * 4);
            
            // Order 3 name length
            byte[] name_length = new byte[4];

            name_length = BitConverter.GetBytes(states[i].stateName.Length);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(name_length);

            Buffer.BlockCopy(name_length, 0, buffer, cursor, name_length.Length);
            cursor += (sizeof(byte) * 4);

            // Order 4 name
            byte[] name = System.Text.Encoding.ASCII.GetBytes(states[i].stateName);

            Buffer.BlockCopy(name, 0, buffer, cursor, name.Length);
            cursor += name.Length;
            
            // Order 5, 6, 7, 8 rect

            byte[] rect_x = new byte[4];

            rect_x = BitConverter.GetBytes(states[i].GUIRect.x);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(rect_x);

            Buffer.BlockCopy(rect_x, 0, buffer, cursor, rect_x.Length);
            cursor += (sizeof(byte) * 4);

            byte[] rect_y = new byte[4];

            rect_y = BitConverter.GetBytes(states[i].GUIRect.y);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(rect_y);

            Buffer.BlockCopy(rect_y, 0, buffer, cursor, rect_y.Length);
            cursor += (sizeof(byte) * 4);

            byte[] rect_w = new byte[4];

            rect_w = BitConverter.GetBytes(states[i].GUIRect.width);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(rect_w);

            Buffer.BlockCopy(rect_w, 0, buffer, cursor, rect_w.Length);
            cursor += (sizeof(byte) * 4);

            byte[] rect_h = new byte[4];

            rect_h = BitConverter.GetBytes(states[i].GUIRect.height);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(rect_h);

            Buffer.BlockCopy(rect_h, 0, buffer, cursor, rect_h.Length);
            cursor += (sizeof(byte) * 4);
           
            // Order 9 conditions_set
            byte[] conditions_set = new byte[4];

            conditions_set = BitConverter.GetBytes(states[i].conditionsSet);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(conditions_set);

            Buffer.BlockCopy(conditions_set, 0, buffer, cursor, conditions_set.Length);
            cursor += (sizeof(byte) * 4);

            for(int c = 0; c < states[i].conditionsSet; ++c)
            {

                byte[] obj_name = System.Text.Encoding.ASCII.GetBytes(states[i].returnEffect[c].gameObjectName);
                byte[] obj_len = new byte[4];
                obj_len = BitConverter.GetBytes(obj_name.Length);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(obj_len);

                // Order 10 obj_len
                Buffer.BlockCopy(obj_len, 0, buffer, cursor, obj_len.Length);
                cursor += obj_len.Length;

                // Order 11 obj_name
                Buffer.BlockCopy(obj_name, 0, buffer, cursor, obj_name.Length);
                cursor += obj_name.Length;

                // Order 12 emotions_count
                byte[] emotion_count = new byte[4];
                emotion_count = BitConverter.GetBytes(states[i].returnEffect[c].conditions.Count);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(emotion_count);

                Buffer.BlockCopy(emotion_count, 0, buffer, cursor, emotion_count.Length);
                cursor += emotion_count.Length;

                for (int emotions = 0; emotions < states[i].returnEffect[c].conditions.Count; ++emotions)
                {
                    //Order 13, 14 Emotion type and emotion level
                    byte[] emotion_type = new byte[4];
                    emotion_type = BitConverter.GetBytes((int)states[i].returnEffect[c].conditions[emotions].emotion);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(emotion_type);

                    Buffer.BlockCopy(emotion_type, 0, buffer, cursor, emotion_type.Length);
                    cursor += emotion_type.Length;

                    byte[] emotion_lev = new byte[4];
                    emotion_lev = BitConverter.GetBytes(states[i].returnEffect[c].conditions[emotions].level);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(emotion_lev);

                    Buffer.BlockCopy(emotion_lev, 0, buffer, cursor, emotion_lev.Length);
                    cursor += emotion_lev.Length;
                }
            }

        }

        // Order 15 Connections count

        Buffer.BlockCopy(connections_count, 0, buffer, cursor, connections_count.Length);
        cursor += connections_count.Length;

        for (int i = 0; i < connections.Count; ++i)
        {
            // Order 16 in_id
            byte[] in_id = new byte[4];
            in_id = BitConverter.GetBytes(connections[i].inPoint.state.id);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(in_id);

            Buffer.BlockCopy(in_id, 0, buffer, cursor, in_id.Length);
            cursor += sizeof(byte) * 4;

            // Order 17 out_id
            byte[] out_id = new byte[4];
            out_id = BitConverter.GetBytes(connections[i].outPoint.state.id);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(out_id);

            Buffer.BlockCopy(out_id, 0, buffer, cursor, out_id.Length);
            cursor += sizeof(byte) * 4;
            
        }

        string pathtosave = EditorUtility.SaveFilePanel("States", Application.streamingAssetsPath, "States", "stale");
        if (pathtosave.Length != 0)
        {
            StreamWriter states_writer = new StreamWriter(pathtosave);
            states_writer.BaseStream.Write(buffer, 0, size);
            states_writer.Flush();
            states_writer.Close();

        }
    }

    public static void Load()
    {
        byte[] data;
        using (StreamReader sr = new StreamReader(pathtoload))
        {
            using (MemoryStream ms = new MemoryStream())
            {
                sr.BaseStream.CopyTo(ms);
                data = ms.ToArray();
            }
        }
        
        int cursor = 0;

        byte[] states_c = new byte[4];
        Buffer.BlockCopy(data, cursor, states_c, 0, 4);

        if (BitConverter.IsLittleEndian)
            Array.Reverse(states_c);

        int states_count = BitConverter.ToInt32(states_c, 0);
        cursor += sizeof(byte) * 4;

        if (states != null)
        {
            states.Clear();
        }
        else
        {
            states = new List<State>();
        }

        for (int i = 0; i < states_count; ++i)
        {

            byte[] states_id = new byte[4];
            Buffer.BlockCopy(data, cursor, states_id, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(states_id);

            int id = BitConverter.ToInt32(states_id, 0);
            cursor += sizeof(byte) * 4;

            byte[] name_length = new byte[4];
            Buffer.BlockCopy(data, cursor, name_length, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(name_length);

            int staten_length = BitConverter.ToInt32(name_length, 0);
            cursor += sizeof(byte) * 4;

            byte[] name = new byte[staten_length];
            Buffer.BlockCopy(data, cursor, name, 0, staten_length);
            
            string state_name = System.Text.Encoding.ASCII.GetString(name);
            cursor += staten_length;

            byte[] rectx = new byte[4];
            byte[] recty = new byte[4];
            byte[] rectw = new byte[4];
            byte[] recth = new byte[4];

            Buffer.BlockCopy(data, cursor, rectx, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(rectx);

            float rec_x = BitConverter.ToSingle(rectx, 0);
            cursor += sizeof(byte) * 4;

            Buffer.BlockCopy(data, cursor, recty, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(recty);

            float rec_y = BitConverter.ToSingle(recty, 0);
            cursor += sizeof(byte) * 4;


            Buffer.BlockCopy(data, cursor, rectw, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(rectw);

            float rec_w = BitConverter.ToSingle(rectw, 0);
            cursor += sizeof(byte) * 4;

            Buffer.BlockCopy(data, cursor, recth, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(recth);

            float rec_h = BitConverter.ToSingle(recth, 0);
            cursor += sizeof(byte) * 4;

            State state = new State(
                id, state_name,
                new Vector2(rec_x, rec_y),
                rec_w, rec_h,
                stateStyle,
                OnClickInPoint, OnClickOutPoint, //Functions to connect states
                selectedNodeStyle,
                OnClickRemoveState
                );


            byte[] conditions = new byte[4];
            Buffer.BlockCopy(data, cursor, conditions, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(conditions);

            int conditions_set = BitConverter.ToInt32(conditions, 0);
            cursor += sizeof(byte) * 4;

            for(int cond = 0; cond < conditions_set; ++cond)
            {
                byte[] obj_length = new byte[4];
                Buffer.BlockCopy(data, cursor, obj_length, 0, 4);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(obj_length);

                int obj_len = BitConverter.ToInt32(obj_length, 0);
                cursor += sizeof(byte) * 4;

                byte[] obj_name = new byte[obj_len];
                Buffer.BlockCopy(data, cursor, obj_name, 0, obj_len);
                string object_name = System.Text.Encoding.ASCII.GetString(obj_name); ;
                cursor += obj_len;

                List<sTaleUtils.Effect> effect = new List<sTaleUtils.Effect>();
                
                byte[] effects_set = new byte[4];
                Buffer.BlockCopy(data, cursor, effects_set, 0, 4);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(effects_set);

                int effects = BitConverter.ToInt32(effects_set, 0);
                cursor += sizeof(byte) * 4;

                for(int eff = 0; eff < effects; ++eff)
                {
                    byte[] emotion_type = new byte[4];
                    Buffer.BlockCopy(data, cursor, emotion_type, 0, 4);

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(emotion_type);

                    int em_type = BitConverter.ToInt32(emotion_type, 0);
                    cursor += sizeof(byte) * 4;


                    byte[] emotion_lev = new byte[4];
                    Buffer.BlockCopy(data, cursor, emotion_lev, 0, 4);

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(emotion_lev);

                    float level = BitConverter.ToSingle(emotion_lev, 0);
                    cursor += sizeof(byte) * 4;
                    
                    effect.Add(new sTaleUtils.Effect((sTaleUtils.EmotionType)em_type, level));
                }

                state.Load(object_name, effect);
            }

            states.Add(state);
        }

        if (connections != null)
        {
            connections.Clear();
        }
        else
        {
            connections = new List<Connection>();
        }


        byte[] connections_c = new byte[4];
        Buffer.BlockCopy(data, cursor, connections_c, 0, 4);

        if (BitConverter.IsLittleEndian)
            Array.Reverse(connections_c);

        int conn_count = BitConverter.ToInt32(connections_c, 0);
        cursor += sizeof(byte) * 4;


        for (int i = 0; i < conn_count; ++i)
        {

            byte[] instate = new byte[4];
            Buffer.BlockCopy(data, cursor, instate, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(instate);

            int in_id = BitConverter.ToInt32(instate, 0);
            cursor += sizeof(byte) * 4;

            byte[] outstate = new byte[4];
            Buffer.BlockCopy(data, cursor, outstate, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(outstate);

            int out_id = BitConverter.ToInt32(outstate, 0);
            cursor += sizeof(byte) * 4;


            connections.Add(new Connection(
                states.Find(s => s.id == in_id).inPoint,
                states.Find(s => s.id == out_id).outPoint,
                OnClickRemoveConnection
                ));
        }


    }

    // Process events

    private void ProcessEvents(Event ev)
    {
        dragCanvas = Vector2.zero;
        if (ev.type == EventType.MouseDown)
        {
            switch (ev.button)
            {
                case 0:
                    GUI.FocusControl(null);
                    break;
                case 1: //Right button down
                    ShowOptions(ev.mousePosition);
                    break;
                
            }
        }
        if (ev.type == EventType.MouseDrag)
        {
            if (ev.button == 0)
            {
                OnDrag(ev.delta);
            }
        }

    }

    private void ProcessStatesEvents(Event ev)
    {
        if (states != null)
        {
            for (int i = states.Count - 1; i >= 0; i--)
            {
                bool guiChanged = states[i].ProcessEvents(ev);

                if (guiChanged)
                {
                    GUI.changed = true;
                }
            }
        }
    }

    // Callback functions
    
    private void ShowOptions(Vector2 mousePosition)
    {
        GenericMenu options_menu = new GenericMenu();
        options_menu.AddItem(new GUIContent("Add state"), false, () => OnClickAddState(mousePosition));
        options_menu.AddItem(new GUIContent("Clear all"), false, () => OnClickClearAll());
        options_menu.ShowAsContext();
    }

    private void OnClickAddState(Vector2 mousePosition)
    {
        if (states == null)
        {
            states = new List<State>();
        }

        states.Add(new State(states.Count, "State " + states.Count, mousePosition, 200, 200, stateStyle, OnClickInPoint, OnClickOutPoint, selectedNodeStyle, OnClickRemoveState));


    }

    private void OnClickClearAll()
    {
        if(states != null)
        {
            if(states.Count > 0)
            {
                for (int i = states.Count - 1; i >= 0; --i)
                {
                    states[i].OnRemoveState?.Invoke(states[i]);
                }
            }
        }
    }

    private void OnDrag(Vector2 delta)
    {
        dragCanvas = delta;

        if (states != null)
        {
            for (int i = 0; i < states.Count; i++)
            {
                states[i].Drag(delta);
            }
        }

        GUI.changed = true;
    }

    private static void OnClickInPoint(ConnectionPoint inPoint)
    {
        selectedInPoint = inPoint;

        if (selectedOutPoint != null)
        {
            if (selectedOutPoint.state != selectedInPoint.state)
            {
                CreateConnection();
                ClearConnectionSelection();
            }
            else
            {
                ClearConnectionSelection();
            }
        }
    }

    private static void OnClickOutPoint(ConnectionPoint outPoint)
    {
        selectedOutPoint = outPoint;

        if (selectedInPoint != null)
        {
            if (selectedOutPoint.state != selectedInPoint.state)
            {
                CreateConnection();
                ClearConnectionSelection();
            }
            else
            {
                ClearConnectionSelection();
            }
        }
    }

    private static void OnClickRemoveConnection(Connection connection)
    {
        connections.Remove(connection);
    }

    private static void CreateConnection()
    {
        if (connections == null)
        {
            connections = new List<Connection>();
        }

        connections.Add(new Connection(selectedInPoint, selectedOutPoint, OnClickRemoveConnection));
    }

    private static void ClearConnectionSelection()
    {
        selectedInPoint = null;
        selectedOutPoint = null;
    }

    private static void OnClickRemoveState(State state)
    {
        if (connections != null)
        {
            List<Connection> connectionsToRemove = new List<Connection>();

            for (int i = 0; i < connections.Count; i++)
            {
                if (connections[i].inPoint == state.inPoint || connections[i].outPoint == state.outPoint)
                {
                    connectionsToRemove.Add(connections[i]);
                }
            }

            for (int i = 0; i < connectionsToRemove.Count; i++)
            {
                connections.Remove(connectionsToRemove[i]);
            }

            connectionsToRemove = null;
        }

        states.Remove(state);
    }
    
}
