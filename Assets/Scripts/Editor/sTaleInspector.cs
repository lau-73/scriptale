﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(sTale))]

public class sTaleInspector : Editor {

    SerializedProperty sTale_inspector;
    SerializedProperty statesFile;
    SerializedProperty relationshipsFile;
    SerializedProperty dialogueImporter;

    void OnEnable () {

        sTale_inspector = serializedObject.FindProperty("sTale_inspector");

        statesFile = serializedObject.FindProperty("statesFile");
        relationshipsFile = serializedObject.FindProperty("relationshipsFile");
        dialogueImporter = serializedObject.FindProperty("dialogueImporter");


        if (sTale.docPath != null)
        {
            sTaleStatesEditor.SetStatesPath(sTale.docPath);
        }

        if (sTale.relDocPath != null)
        {
            sTaleRelationshipsEditor.SetRelationshipsPath(sTale.relDocPath);
        }
	}

    
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(sTale_inspector);

        EditorGUILayout.PropertyField(dialogueImporter);

        GUILayout.Label("States Editor");
        //Button will open the editor
        EditorGUI.BeginChangeCheck();

        EditorGUILayout.PropertyField(statesFile);

        if (EditorGUI.EndChangeCheck())
        {
            if (sTale.docPath != null)
            {
                sTaleStatesEditor.SetStatesPath(sTale.docPath);
            }
        }

        if (GUILayout.Button("Open Editor"))
            sTaleStatesEditor.OpenEditor();

        GUILayout.Label("Relationships Editor");
        //Button will open the editor
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(relationshipsFile);

        if (EditorGUI.EndChangeCheck())
        {
            if (sTale.relDocPath != null)
            {
                sTaleRelationshipsEditor.SetRelationshipsPath(sTale.relDocPath);
            }
        }

        if (GUILayout.Button("Open Editor"))
            sTaleRelationshipsEditor.OpenEditor();
        
        serializedObject.ApplyModifiedProperties();

    }

}
