﻿using System.Collections.Generic;
using UnityEngine;

public class sTaleRelation {

    public sTaleCharacter char1;
    public sTaleCharacter char2;

    public List<sTaleUtils.Effect> influence;

    public sTaleRelation() { }
    public sTaleRelation(sTaleCharacter character1, sTaleCharacter character2)
    {
        char1 = character1;
        char2 = character2;
    }

    public sTaleRelation(sTaleCharacter character1, sTaleCharacter character2, List<sTaleUtils.Effect> effect)
    {
        char1 = character1;
        char2 = character2;

        if(influence == null)
        {
            influence = new List<sTaleUtils.Effect>(effect);
        }

        else
        {
            influence.Clear();
            influence = effect;
        }
    }

    public void LoadInfluence(List<sTaleUtils.Effect> effect)
    {
        if (influence == null)
        {
            influence = new List<sTaleUtils.Effect>(effect);
        }

        else
        {
            influence.Clear();
            influence = effect;
        }
    }

    public void ModifyInfluence(sTaleUtils.Effect modification)
    {
        sTaleUtils.Effect effect = influence.Find(e => e.emotion == modification.emotion);

        if (effect == null)
            influence.Add(new sTaleUtils.Effect(modification.emotion, Mathf.Clamp(modification.level * 0.1f, 0.0F, 100.0F)));

        else
        {
            effect.level += (modification.level * 0.1f);
            effect.level = Mathf.Clamp(effect.level, 0.0F, 100.0F);
        }
        
    }

}
