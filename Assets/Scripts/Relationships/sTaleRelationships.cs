﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class sTaleRelationships {

    public static List<sTaleRelation> characters_relationships;

    public static void Load(string pathtoload)
    {
        if(characters_relationships == null)
        {
            characters_relationships = new List<sTaleRelation>();
        }
        characters_relationships.Clear();

        byte[] data;
        using (StreamReader sr = new StreamReader(pathtoload))
        {
            using (MemoryStream ms = new MemoryStream())
            {
                sr.BaseStream.CopyTo(ms);
                data = ms.ToArray();
            }
        }

        int cursor = 0;

        byte[] char_c = new byte[4];
        Buffer.BlockCopy(data, cursor, char_c, 0, 4);

        if (BitConverter.IsLittleEndian)
            Array.Reverse(char_c);

        int char_count = BitConverter.ToInt32(char_c, 0);
        cursor += sizeof(byte) * 4;
        
        for (int i = 0; i < char_count; ++i)
        {

            byte[] char_id = new byte[4];
            Buffer.BlockCopy(data, cursor, char_id, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(char_id);

            //int id = BitConverter.ToInt32(char_id, 0);
            cursor += sizeof(byte) * 4;

            byte[] name_length = new byte[4];
            Buffer.BlockCopy(data, cursor, name_length, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(name_length);

            int char_name_len = BitConverter.ToInt32(name_length, 0);
            cursor += sizeof(byte) * 4;
            cursor += char_name_len;

            cursor += sizeof(byte) * 4 * 4; //rect
        }
        
        byte[] connections_c = new byte[4];
        Buffer.BlockCopy(data, cursor, connections_c, 0, 4);

        if (BitConverter.IsLittleEndian)
            Array.Reverse(connections_c);

        int conn_count = BitConverter.ToInt32(connections_c, 0);
        cursor += sizeof(byte) * 4;

        sTaleCharacter[] characters_go = GameObject.FindObjectsOfType<sTaleCharacter>();

        for (int i = 0; i < conn_count; ++i)
        {

            byte[] instate = new byte[4];
            Buffer.BlockCopy(data, cursor, instate, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(instate);

            int in_id = BitConverter.ToInt32(instate, 0);
            cursor += sizeof(byte) * 4;

            byte[] outstate = new byte[4];
            Buffer.BlockCopy(data, cursor, outstate, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(outstate);

            int out_id = BitConverter.ToInt32(outstate, 0);
            cursor += sizeof(byte) * 4;

            characters_relationships.Add(new sTaleRelation());

            byte[] inf_c = new byte[4];
            Buffer.BlockCopy(data, cursor, inf_c, 0, inf_c.Length);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(inf_c);

            int influence_count = BitConverter.ToInt32(inf_c, 0);
            cursor += sizeof(byte) * 4;

            sTaleCharacter char1 = null;
            sTaleCharacter char2 = null;

            for (int charac = 0; charac < characters_go.Length || (char1 == null || char2 == null); ++charac)
            {
                if (characters_go[charac].identifier == in_id)
                {
                    char1 = characters_go[charac];
                }

                if (characters_go[charac].identifier == out_id)
                {
                    char2 = characters_go[charac];
                }
            }
            
            List<sTaleUtils.Effect> influence = new List<sTaleUtils.Effect>();

            for (int inf = 0; inf < influence_count; ++inf)
            {
                byte[] emotion_type = new byte[4];
                Buffer.BlockCopy(data, cursor, emotion_type, 0, emotion_type.Length);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(emotion_type);

                int em_type = BitConverter.ToInt32(emotion_type, 0);
                cursor += emotion_type.Length;

                byte[] emotion_lev = new byte[4];
                Buffer.BlockCopy(data, cursor, emotion_lev, 0, emotion_lev.Length);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(emotion_lev);

                float emotion_level = BitConverter.ToSingle(emotion_lev, 0);
                cursor += emotion_lev.Length;

                influence.Add(new sTaleUtils.Effect((sTaleUtils.EmotionType)em_type, emotion_level));
            }

            characters_relationships.Add(new sTaleRelation(char1, char2, influence));

        }
    }

}
