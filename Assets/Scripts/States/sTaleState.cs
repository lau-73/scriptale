﻿using System.Collections.Generic;
using UnityEngine;


public class Conditions<Type>
{
    public string gameObjectName;
    public List<Type> conditions;

    public Conditions (string name)
    {
        gameObjectName = name;
        conditions = new List<Type>();
    }

    public Conditions()
    {   
        conditions = new List<Type>();
    }
}

public class sTaleState {

    public int id;

    public List<GameObject> objectRequirements;

    public int conditionsSet = 0;
    public List<Conditions<sTaleUtils.Effect>> returnEffect;
    
    public sTaleState()
    { }

    public sTaleState(int state_id)
    {
        id = state_id;        
    }

    public void Load(string go_name, List<sTaleUtils.Effect> effects)
    {
        if (objectRequirements == null)
            objectRequirements = new List<GameObject>();

        objectRequirements.Add(GameObject.Find(go_name));

        if (returnEffect == null)
            returnEffect = new List<Conditions<sTaleUtils.Effect>>();

        Conditions<sTaleUtils.Effect> obj_effects = new Conditions<sTaleUtils.Effect>();

        obj_effects.gameObjectName = go_name;
        obj_effects.conditions = effects;

        returnEffect.Add(obj_effects);

        conditionsSet++;
    }
}
