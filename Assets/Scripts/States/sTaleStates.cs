﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class sTaleStates {

    public static sTaleTree<sTaleState> states_tree;
    private static Leaf<sTaleState> current_state;
    private static Leaf<sTaleState> following_state;

    public static void Load(string pathtoload)
    {
        List<sTaleState> temp_states = new List<sTaleState>();
        List<sTaleConnection> temp_connection = new List<sTaleConnection>();

        byte[] data;
        using (StreamReader sr = new StreamReader(pathtoload))
        {
            using (MemoryStream ms = new MemoryStream())
            {
                sr.BaseStream.CopyTo(ms);
                data = ms.ToArray();
            }
        }

        int cursor = 0;

        byte[] states_c = new byte[4];
        Buffer.BlockCopy(data, cursor, states_c, 0, 4);

        if (BitConverter.IsLittleEndian)
            Array.Reverse(states_c);

        int states_count = BitConverter.ToInt32(states_c, 0);
        cursor += sizeof(byte) * 4;
        
        for (int i = 0; i < states_count; ++i)
        {

            byte[] states_id = new byte[4];
            Buffer.BlockCopy(data, cursor, states_id, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(states_id);

            int id = BitConverter.ToInt32(states_id, 0);
            cursor += sizeof(byte) * 4;

            byte[] name_length = new byte[4];
            Buffer.BlockCopy(data, cursor, name_length, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(name_length);

            int staten_length = BitConverter.ToInt32(name_length, 0);
            cursor += sizeof(byte) * 4; //We won't make use of name length though, just to know the following number of bytes to skip
            
            cursor += staten_length; //We skip the name
            cursor += (sizeof(byte) * 4 * 4); //And also the rectangle

            //We can now, deserialize the state
            sTaleState state = new sTaleState(id);

            //Let's fill it's conditions
            
            byte[] conditions = new byte[4];
            Buffer.BlockCopy(data, cursor, conditions, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(conditions);

            int conditions_set = BitConverter.ToInt32(conditions, 0);
            cursor += sizeof(byte) * 4;

            for (int cond = 0; cond < conditions_set; ++cond)
            {
                byte[] obj_length = new byte[4];
                Buffer.BlockCopy(data, cursor, obj_length, 0, 4);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(obj_length);

                int obj_len = BitConverter.ToInt32(obj_length, 0);
                cursor += sizeof(byte) * 4;

                byte[] obj_name = new byte[obj_len];
                Buffer.BlockCopy(data, cursor, obj_name, 0, obj_len);
                string object_name = System.Text.Encoding.ASCII.GetString(obj_name); ;
                cursor += obj_len;

                List<sTaleUtils.Effect> effect = new List<sTaleUtils.Effect>();

                byte[] effects_set = new byte[4];
                Buffer.BlockCopy(data, cursor, effects_set, 0, 4);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(effects_set);

                int effects = BitConverter.ToInt32(effects_set, 0);
                cursor += sizeof(byte) * 4;

                for (int eff = 0; eff < effects; ++eff)
                {
                    byte[] emotion_type = new byte[4];
                    Buffer.BlockCopy(data, cursor, emotion_type, 0, 4);

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(emotion_type);

                    int em_type = BitConverter.ToInt32(emotion_type, 0);
                    cursor += sizeof(byte) * 4;


                    byte[] emotion_lev = new byte[4];
                    Buffer.BlockCopy(data, cursor, emotion_lev, 0, 4);

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(emotion_lev);

                    float level = BitConverter.ToSingle(emotion_lev, 0);
                    cursor += sizeof(byte) * 4;

                    effect.Add(new sTaleUtils.Effect((sTaleUtils.EmotionType)em_type, level));
                }

                state.Load(object_name, effect);
            }

            temp_states.Add(state);
        }
        
        byte[] connections_c = new byte[4];
        Buffer.BlockCopy(data, cursor, connections_c, 0, 4);

        if (BitConverter.IsLittleEndian)
            Array.Reverse(connections_c);

        int conn_count = BitConverter.ToInt32(connections_c, 0);
        cursor += sizeof(byte) * 4;
        
        for (int i = 0; i < conn_count; ++i)
        {
            byte[] instate = new byte[4];
            Buffer.BlockCopy(data, cursor, instate, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(instate);

            int in_id = BitConverter.ToInt32(instate, 0);
            cursor += sizeof(byte) * 4;

            byte[] outstate = new byte[4];
            Buffer.BlockCopy(data, cursor, outstate, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(outstate);

            int out_id = BitConverter.ToInt32(outstate, 0);
            cursor += sizeof(byte) * 4;

            temp_connection.Add(new sTaleConnection(temp_states.Find(s => s.id == in_id), temp_states.Find(s => s.id == out_id)));
                        
        }

        // connection whose parent does not appear in the list anymore
        sTaleState root = new sTaleState();

        if (temp_connection.Count == 0)
        {
            Debug.LogError("Please, add more states to the tree");
        }

        for (int parent = 0; parent < temp_connection.Count; ++parent)
        {
            bool match = false;

            for (int child = 0; child < temp_connection.Count; ++child)
            {
                if (temp_connection[parent].parent == temp_connection[child].children)
                {
                    match = true;
                }
            }

            if (match == false)
            {
                root = temp_connection[parent].parent;
                break;
            }
        }

        states_tree = new sTaleTree<sTaleState>();
        
        CreateTreeFrom(root, null, temp_connection);

    }

    private static void CreateTreeFrom(sTaleState state, Leaf<sTaleState> parent, List<sTaleConnection> connections)
    {
        Leaf<sTaleState> node = states_tree.AddNode(state, parent);

        List<sTaleConnection> children = connections.FindAll(c => c.parent == state);

        for (int i = 0; i < children.Count; ++i)
        {
            CreateTreeFrom(children[i].children, node, connections);
        }
    }

    public static void Init()
    {
        current_state = states_tree.GetRoot();
        GetNextState();
        
    }

    private static void GetNextState()
    {
        if (current_state.children == null) return;
        else if (current_state.children.Count == 0) return;
        
        int following_index = 0;
        
        float cost = 0.0f;
        float last_cost = -1.0f;

        for(int current_char = 0; current_char <  current_state.children.Count; ++current_char)
        {

            for(int obj = 0; obj < current_state.children[current_char].content.objectRequirements.Count; ++obj)
            {
                for (int e = 0; e < current_state.children[current_char].content.returnEffect.Count; ++e)
                {
                    for (int c = 0; c < current_state.children[current_char].content.returnEffect[e].conditions.Count; ++c)
                    {
                        sTaleUtils.Effect res_effect = Array.Find(current_state.children[current_char].content.objectRequirements[obj].GetComponent<sTaleCharacter>().currentState, em => em.emotion == current_state.children[current_char].content.returnEffect[e].conditions[c].emotion);

                        if (res_effect != null)
                            cost += Mathf.Abs(res_effect.level - current_state.children[current_char].content.returnEffect[e].conditions[c].level);
                    }
                }

            }


            if (last_cost > -1.0f)
            {
                if (last_cost < cost)
                {
                    following_index = current_char;
                }
            }

            last_cost = cost;
            cost = 0.0f;

        }

        following_state = current_state.children[following_index];
    }

    public static List<Conditions<sTaleUtils.Effect>> GetConditions()
    {
        return current_state.content.returnEffect;
    }

    public static bool UpdateState()
    {
        bool ret = true;

        sTaleCharacter[] characters = GameObject.FindObjectsOfType<sTaleCharacter>();

        for (int i = 0; i < current_state.content.conditionsSet; ++i)
        {
            sTaleCharacter validate_char = Array.Find(characters, character => character.name == current_state.content.objectRequirements[i].name);

            for (int c = 0; c < current_state.content.returnEffect[i].conditions.Count; ++c)
            {
                sTaleUtils.Effect validate_eff = Array.Find(validate_char.currentState, s => s.emotion == current_state.content.returnEffect[i].conditions[c].emotion);

                if (validate_eff == null || Mathf.Abs(validate_eff.level - current_state.content.returnEffect[i].conditions[c].level) > 2.0f)
                {
                    ret = false;
                }

                else
                {
                    current_state.content.returnEffect[i].conditions.Remove(current_state.content.returnEffect[i].conditions[c]);
                }
            }
        }

        if(ret)
        {
            current_state = following_state;
            GetNextState();
        }

        return ret;
    }
}
