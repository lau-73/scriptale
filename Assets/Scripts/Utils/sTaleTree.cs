﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leaf<Type>
{
    public Leaf(Type info, Leaf<Type> pr)
    {
        parent = pr;

        if(pr != null)
        {
            if (pr.children == null)
                pr.children = new List<Leaf<Type>>();

            ch_index = pr.children.Count;
            pr.children.Add(this);
        }
        
        content = info;
    }

    public List<Leaf<Type>> children;
    Leaf<Type> parent;
    public int ch_index;

    public Type content;
}

public class sTaleTree<Type> {

    private Leaf<Type> root;

    public sTaleTree()
    { }

    public Leaf<Type> AddNode(Type content, Leaf<Type> parent)
    {
        Leaf<Type> node = new Leaf<Type>(content, parent);

        if (root == null)
            root = node;

        return node;
    }

    public Leaf<Type> GetRoot()
    {
        return root;
    }
}