﻿using ScripTale;
using System.IO;
using UnityEditor;
using UnityEngine;

public class sTale : MonoBehaviour {

    public int sTale_inspector = 0;
    
    [SerializeField]
    public DefaultAsset statesFile;
    [SerializeField]
    public DefaultAsset relationshipsFile;
    [SerializeField]
    public GameObject dialogueImporter;

    public static string docPath;
    public static string relDocPath;

    
    void Start () {

        dialogueImporter.GetComponent<DialogueImporter>().ReadDialogue();

        if (statesFile != null)
        {
            sTaleStates.Load(Path.Combine(Application.dataPath, statesFile.name + ".stale"));
        }
        else
        {
            Debug.LogError("Please add a correct states file");
            return;
        }
            

        if (relationshipsFile != null)
            sTaleRelationships.Load(Path.Combine(Application.dataPath, relationshipsFile.name + ".rstale"));

        else
        {
            Debug.LogError("Please add a correct relationships file");
            return;
        }

        sTaleStates.Init();

    }
    

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (statesFile != null)
            docPath = AssetDatabase.GetAssetPath(statesFile);

        if (relationshipsFile != null)
            relDocPath = AssetDatabase.GetAssetPath(relationshipsFile);
    }
# endif

}
