﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;

using static sTaleUtils;

[Serializable]
public class AvailableAction
{
    public int intensity;
    public UnityEvent action;
}

[Serializable]
public class InteractEvent : UnityEvent<sTaleCharacter> { }

[Serializable]
public class TalkEvent : UnityEvent<string> { }

[Serializable]
public class sTaleCharacter : MonoBehaviour {
    
    [SerializeField] public int identifier;

    [SerializeField] public Effect[] baseBehaviour;
    [SerializeField] public Message[] characterMessages;

    public Effect[] currentState;
    List<Effect> memory;
    
    [SerializeField] public TalkEvent talkAction;
    [SerializeField] public InteractEvent conditionToInteract;

    [SerializeField] List<AvailableAction> availableActions;
    [SerializeField] float actionLastingTime;
    
    [SerializeField] int affectsRelationship = 2;

    private CharacterState characterState = CharacterState.Idle;
    public bool canInteract = false;

    // Use this for initialization
    void Start () {
        memory = new List<Effect>();
        currentState = baseBehaviour;

    }
	
	void Update () {

        if (talkAction == null || availableActions == null) return;

        if(characterState == CharacterState.Idle)
        {
            ActFromGoal();
        }
        
	}


    void ActFromGoal()
    {

        List<Conditions<sTaleUtils.Effect>> conditions = sTaleStates.GetConditions();
        for (int i = 0; i < conditions.Count; ++i)
        {
            for (int m = 0; m < conditions[i].conditions.Count; ++m)
            {
                sTaleCharacter char_to_interact = GameObject.Find(conditions[i].gameObjectName).GetComponent<sTaleCharacter>();

                if (char_to_interact == null || char_to_interact == this || !char_to_interact.GetCondition(this)) continue;

                characterState = CharacterState.Act;
                /*float modify_level = 0;

                if (relation != null)
                {
                    Effect relationship_modify = relation.influence.Find(e => e.emotion == conditions[i].conditions[m].emotion);

                    if (relationship_modify != null)
                    {
                        modify_level = relationship_modify.level;
                    }
                }*/


                Message[] suitable_mess = Array.FindAll(characterMessages, mess => mess.effect.emotion == conditions[i].conditions[m].emotion);

                if (suitable_mess == null)
                {
                    continue;
                }

                Message[] return_effect = Array.FindAll(suitable_mess, sm => Mathf.Abs(sm.effect.level - conditions[i].conditions[m].level) < 4.0f);// suitable_mess[0].effect;

                if (return_effect == null)
                {
                    continue;
                }

                int rnd_effect = UnityEngine.Random.Range(0, return_effect.Length);

                return_effect[rnd_effect].effect.SetFrom(identifier);
                return_effect[rnd_effect].effect.SetTo(char_to_interact.identifier);

                AvailableAction action = GetAction((int)conditions[i].conditions[m].level);
                Act(action, return_effect[rnd_effect].effect, UnityEngine.Random.Range(0, 4));
                return;
            }
        }

    }

    AvailableAction GetAction(int action_intensity)
    {
        if (availableActions != null)
        {
            AvailableAction action = availableActions.Find(act => act.intensity == action_intensity);

            if (action == null)
            {
                action = availableActions[0];

                foreach (AvailableAction a in availableActions)
                {
                    if (Mathf.Abs(a.intensity - action_intensity) < Mathf.Abs(action.intensity - action_intensity))
                        action = a;
                }

                if (action != null)
                    action.action?.Invoke();
            }

            return action;
        }

        return null;
    }

    bool GetCondition(sTaleCharacter other)
    {

        MethodInfo methodInfo = UnityEventBase.GetValidMethodInfo(conditionToInteract.GetPersistentTarget(0), conditionToInteract.GetPersistentMethodName(0), new System.Type[] { typeof(sTaleCharacter) });
        
        methodInfo.Invoke(conditionToInteract.GetPersistentTarget(0), new object[] { other });
        /*
        conditionToInteract.SetPersistentListenerState(0, UnityEventCallState.Off);S
        conditionToInteract.RemoveAllListeners();

        MethodInfo condition_method = UnityEventBase.GetValidMethodInfo(conditionToInteract.GetPersistentTarget(0), conditionToInteract.GetPersistentMethodName(0), new System.Type[] { typeof(sTaleCharacter), typeof(bool) });
        bool can_interact = false;
        condition_method.Invoke(gameObject, new object[] { other, can_interact });
        */
        return other.canInteract;
    }

    public void Act(AvailableAction action, Effect effect_on_character, int duration)
    {
        if(characterState != CharacterState.Act)
            characterState = CharacterState.Act;

        sTaleRelation relation = sTaleRelationships.characters_relationships.Find(r => (r.char1.identifier == identifier && r.char2.identifier == effect_on_character.GetTo()) || (r.char1.identifier == effect_on_character.GetTo() && r.char2.identifier == identifier));

        float modify_level = 0;

        if (relation != null)
        {
            Effect relationship_modify = relation.influence.Find(e => e.emotion == effect_on_character.emotion);

            if (relationship_modify != null)
            {
                modify_level = relationship_modify.level;
            }
        }

        
        Message[] messages = Array.FindAll(characterMessages, m => m.effect.emotion == effect_on_character.emotion);

        if(messages != null)
        {

            Message[] message = Array.FindAll(messages, mess => Mathf.Abs(effect_on_character.level - mess.effect.level) < 3.0f);

            if(message != null)
            {
                Message toTalk = message[UnityEngine.Random.Range(0, message.Length)];
                toTalk.message = toTalk.message.Insert(0, Assets.Scripts.Dialogue.GetMessagesByEmotionAndLevel(toTalk.effect.emotion, toTalk.effect.level, 20.0f)[0].message + " ");
                Talk(toTalk);
            }
        }

        float action_intensity = 0;

        if(action.action != null)
        {
            action.action?.Invoke();
            action_intensity = action.intensity;
        }

        sTaleCharacter toInteract = Array.Find(FindObjectsOfType<sTaleCharacter>(), c => c.identifier == effect_on_character.GetTo());
        
        toInteract.Interact(this, (int)action_intensity, effect_on_character, duration);
    }

    void Talk(Message message)
    {

        if (talkAction == null) return;
        MethodInfo methodInfo = UnityEventBase.GetValidMethodInfo(talkAction.GetPersistentTarget(0), talkAction.GetPersistentMethodName(0), new System.Type[] { typeof(string) });

        methodInfo.Invoke(talkAction.GetPersistentTarget(0), new object[] { message.message });

        StartCoroutine("TalkCoroutine");
    }

    IEnumerator TalkCoroutine()
    {
        yield return new WaitForSeconds(actionLastingTime);
        characterState = CharacterState.Idle;
    }

    void Interact(sTaleCharacter you, int action_intensity, Effect toMemory, int duration)
    {
        
        memory.Add(toMemory);
        List<Effect> char_mem = memory.FindAll(mem => mem.GetFrom() == toMemory.GetFrom());
        if(char_mem.Count > 10)
        {
            // Remove memory
            memory.Remove(char_mem.Find(eff => eff.level < 20.0f));
        }

        sTaleRelation relation = sTaleRelationships.characters_relationships.Find(r => (r.char1.identifier == toMemory.GetFrom() && r.char2.identifier == identifier) || (r.char2.identifier == toMemory.GetFrom() && r.char1.identifier == identifier));
        if (relation == null)
        {
            sTaleRelationships.characters_relationships.Add(new sTaleRelation(this, you));
        }

        if (char_mem.Count > affectsRelationship)
        {
            
            for (int i = 0; i < char_mem.Count; ++i)
            {
                relation.ModifyInfluence(char_mem[i]);
   
            }
        }

        Effect character_state = Array.Find(currentState, e => e.emotion == toMemory.emotion);
        Effect relationship_modify = relation.influence.Find(e => e.emotion == toMemory.emotion);

        float modify_level = 0;
        if(relationship_modify != null)
        {
            modify_level = relationship_modify.level;
        }

        character_state.level += (toMemory.level + modify_level);
        character_state.level = Mathf.Clamp(character_state.level, 0.0F, 100.0F);

        sTaleStates.UpdateState();

        if (duration > 0)
        {
            //Mirror the action and replying with same intensity
            Effect reaction = toMemory;

            reaction.SetFrom(identifier);
            reaction.SetTo(you.identifier);
            
            you.Act(you.GetAction(action_intensity), reaction, --duration);
        }

    }

}
