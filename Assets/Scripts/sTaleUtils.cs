﻿public static class sTaleUtils {
    
    public enum EmotionType
    {
        Grief,
        Loathing,
        Rage,
        Vigilance,
        Ecstasy,
        Admiration,
        Terror,
        Amazement,
        None
    }

    public enum CharacterState
    {
        Idle,
        Act
    }

    [System.Serializable]
    public class Effect
    {
        private int from;
        private int to;

        public EmotionType emotion;
        public float level;
        public Effect() { }
        public Effect(EmotionType feeling, float intensity)
        {
            emotion = feeling;
            level = intensity;

            from = -1;
            to = -1;
        }

        public void SetEmotion(EmotionType feeling)
        {
            emotion = feeling;
        }

        public void SetIntensity(float intensity)
        {
            level = intensity;
        }
        
        public void SetFrom(int from_char)
        {
            from = from_char;
        }

        public int GetFrom()
        {
            return from;
        }

        public int GetTo()
        {
            return to;
        }

        public void SetTo(int to_char)
        {
            to = to_char;
        }
    }

    [System.Serializable]
    public struct Message
    {
        public Message(Effect message_effect, string content)
        {
            message = content;
            effect = message_effect;
        }

        public string message;

        public Effect effect;

    }
}
