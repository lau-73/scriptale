![](https://i.imgur.com/t7ws29A.png)

## What is ScripTale?

ScripTale is a tool for unity projects to add Procedural Narrative to a Game. Characters will plot their own story by acting one way or another and will
interact to others with messages depending on their mood.

---
## Requirements

The tool has been developed throughout Unity 2017.3.1f1 so, this is the recommended version to use it.
However, it has proved to work in lower versions, always higher than the 2017.0 because it makes use of the Scripting Runtime Version .NET 4.6.

---
## Quick overview of the tool

The tool looks like the following.

![](https://i.imgur.com/V310ntA.png)

ScripTale package already contains a ScripTale prefab with the scrips on the editor on it, once it is added to the scene, the user can start using all the editors.

**States Editor**
States editor is meant to set conditions to occur. The narrative will follow a designed path of the states editor on its creation.
If a condition of a state has not occured, the story must have to complete it before going on to another state.
The states editor looks like the following image:

![](https://i.imgur.com/k8epGgh.png)

**Relationships Editor**
This last editor has been created to endow the narrative with a bit of realism. The effect of a message or action received by a player is slighly modified depending on the character that has communicated it.
Relationships can also be modified along the story. The picture bellow shows the relationship of two characters in the Relationships editor.

![](https://i.imgur.com/Ra0r6hA.png)

---
## Before using it

The dialogue system needs a document to endorse the story with realims. A document with petwords classified into eight categories in their order of importance.
The document MUST be google docs files, uploaded in a Google Drive Account. The document can be downloaded at any time from Unity, with no need to open Google Drive to do so.

- How to write the Document

Write a list with dashes the eight categories as spelled below, with its correspondant capital letters at the beginning:

		Grief
        Loathing
        Rage
        Vigilance
        Ecstasy
        Admiration
        Terror
        Amazement
		
After each emotion, and without listing it with a dash in the list, insert all the petwords corresponding to that category below.
	
![](https://i.imgur.com/7rpoMo6.png)

---
## To begin with ScripTale


1. Download the package provided and import it to Unity:

Import it by simply dragging it to the project or by selecting it from Assets->Import Package->Custom Package.


2. Put the Dialogue Importer found in the Prefabs folder into the scene:

After placing the dialogue importer in the scene, introduce the name (only the name, NOT THE PATH) of the document required and click to the Download toggle.
For the first time, ScripTale will ask to access to the Google Account where the documents are found. So, just agree.
If the dialogue has been updated in Google Drive, simply click the toggle to download it again.
To switch the account, delete the .STale_Credentials folder that has been automatically created in your Documents and you will be asked again to sign in to another account at the time you download the document.


3. Put all the characters in the scene and fill the required filds:

All the characters must have the sTaleCharacter script attached. There is already a prefab with this script attached in the prefabs' folder.
All of them must have different names and ids.

Each character has got the following fields to fill:

identifier: Number to identificate every character. It has to be assigned by the designer and cannot be repeated in different characters. This id number is used to look up for a character in a most efficient and quick way than making a search by the name if the occasion can allow it.

baseBehaviour: Original mood of the character. It is the starting point to modify its mood over the story.

characterMessages: These are all the messages that can be said by a character. They have to be inserted by the designer and classify by category. Messages contain a string, the text that will appear on making a character talk. Each of them will cause an effect, an emotion with its linked intensity to a character. All of this is supplied by the sTale prefab to be specified on it.

affectsRelationship: Variable strongly related to the memory. The messages will affect to the relationship from the number of messages received that can affect relationship. In other words, a relationship is not being changed at every single interaction but at a certain number of interactions that can affect it; this number of interactions correspond to this variable: affectsRelationship. It is set by default, however, it can be changed by the designer in the inspector of sTale.

talkAction: It is the action on talking. It refers on an action that the character could do on talking, for instance, an animation, but not a more complex one. It is assigned by the designer as it will depend on the particular script of each game to control the character.

availableActions: These are all the actions the character can perform in the game that the designer wants the AI to take control of them. Some of them could be to Hit, to Jump, to Run… MonoBehaviour functions that are called on character’s interaction in function of the intensity of the message.

actionLastingTime: To control the pacing of the story and avoid instantaneous responses, the actionLastingTime controls the time it takes this character to act. It can’t interact again until the time has come to its end. It is done to avoid a very quick narrative that can’t even be noticed. Time in this variable has to be specified in seconds, as it will be called by a coroutine to wait until the character can go to the idle state after acting.

conditionToInteract: Characters will only interact with the others if the condition of interaction is accomplished. This condition may vary from a game to the other, as well as the actions that characters can develop, so it is up to the user to assign in the inspector this condition function or the available actions of each character from their own MonoBehaviour script attached to the same Character GameObject.
The user has to be careful, though to send the sTaleCharacter to interact implementing a function which sends the object of the character who wants to interact with the other.

	
4. Create the the States File and the Relationships File with the editors supplied
After saving the desired files, attach them to the suitable field in the ScripTale's inspector.

---
## Editor controls

- States Editor

1. Right-click in the canvas to see general options: Add state, Clear All. Select the desired option.
2. Click a state to select it. Right-click a selected state to see option delete.
3. Link to states by connecting the line that appears on clicking one of the two black squares to another state's.
4. Delete the connection clicking on the square in the middle of the connection line.

- Relationships Editor

1. All the characters will already appear in the editor. Just connect them with each others and the parameters to set the relationship will automatically appear.
2. To connect two characters, select the first one and Enter. A line to connect will appear. Click the character to which you will like to connect it.
3. Right-click in the canvas to see the Clear All connections option.
4. Delete the connection clicking on the square in the middle of the connection line.

